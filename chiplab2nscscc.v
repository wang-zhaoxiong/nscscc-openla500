module mycpu_top(
    input    [ 7:0] ext_int, 
    input           aclk,
    input           aresetn,

    //AXI interface 
    //read reqest
    output   [ 3:0] arid,
    output   [31:0] araddr,
    output   [ 7:0] arlen,
    output   [ 2:0] arsize,
    output   [ 1:0] arburst,
    output   [ 1:0] arlock,
    output   [ 3:0] arcache,
    output   [ 2:0] arprot,
    output          arvalid,
    input           arready,
    //read back
    input    [ 3:0] rid,
    input    [31:0] rdata,
    input    [ 1:0] rresp,
    input           rlast,
    input           rvalid,
    output          rready,
    //write request
    output   [ 3:0] awid,
    output   [31:0] awaddr,
    output   [ 7:0] awlen,
    output   [ 2:0] awsize,
    output   [ 1:0] awburst,
    output   [ 1:0] awlock,
    output   [ 3:0] awcache,
    output   [ 2:0] awprot,
    output          awvalid,
    input           awready,
    //write data
    output   [ 3:0] wid,
    output   [31:0] wdata,
    output   [ 3:0] wstrb,
    output          wlast,
    output          wvalid,
    input           wready,
    //write back
    input    [ 3:0] bid,
    input    [ 1:0] bresp,
    input           bvalid,
    output          bready,

   output [31:0] debug_wb_pc,
   output [ 3:0] debug_wb_rf_we,
   output [ 4:0] debug_wb_rf_wnum,
   output [31:0] debug_wb_rf_wdata
);

core_top chiplab_top(
  .aclk         (aclk      ),
  .intrpt       (ext_int   ),  //232 only 5bit

  .aresetn      (aresetn   ),
  .arid         (arid[3:0] ),
  .araddr       (araddr    ),
  .arlen        (arlen     ),
  .arsize       (arsize    ),
  .arburst      (arburst   ),
  .arlock       (arlock    ),
  .arcache      (arcache   ),
  .arprot       (arprot    ),
  .arvalid      (arvalid   ),
  .arready      (arready   ),
  .rid          (rid[3:0]  ),
  .rdata        (rdata     ),
  .rresp        (rresp     ),
  .rlast        (rlast     ),
  .rvalid       (rvalid    ),
  .rready       (rready    ),
  .awid         (awid[3:0] ),
  .awaddr       (awaddr    ),
  .awlen        (awlen     ),
  .awsize       (awsize    ),
  .awburst      (awburst   ),
  .awlock       (awlock    ),
  .awcache      (awcache   ),
  .awprot       (awprot    ),
  .awvalid      (awvalid   ),
  .awready      (awready   ),
  .wid          (wid[3:0]  ),
  .wdata        (wdata     ),
  .wstrb        (wstrb     ),
  .wlast        (wlast     ),
  .wvalid       (wvalid    ),
  .wready       (wready    ),
  .bid          (bid[3:0]  ),
  .bresp        (bresp     ),
  .bvalid       (bvalid    ),
  .bready       (bready    ),

  .ws_valid     (          ),
  .break_point  (1'b0      ),
  .infor_flag   (1'b0      ),
  .reg_num      (5'b0      ),
  .rf_rdata     (          ),

  .debug0_wb_pc        (debug_wb_pc      ),
  .debug0_wb_rf_wen    (debug_wb_rf_we   ),
  .debug0_wb_rf_wnum   (debug_wb_rf_wnum ),
  .debug0_wb_rf_wdata  (debug_wb_rf_wdata),
  .debug0_wb_inst      (                 )
);
    
endmodule